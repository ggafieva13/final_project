# %%
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import os
import pickle
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, BatchNormalization, Dropout
from sklearn.metrics import classification_report
import keras
from tensorflow.keras.utils import to_categorical

# %% [markdown]
# # Загрузка данных

# %%
_CIFAR_IMAGE_SIZE = 32
_CIFAR_IMAGE_SHAPE = (_CIFAR_IMAGE_SIZE, _CIFAR_IMAGE_SIZE, 3)
EPOCHS = 20
BATCH_SIZE = 100
NUM_F_CLASSES = 100
NUM_C_CLASSES = 20
opt = 'adam'
loss = 'categorical_crossentropy'
metrics = ['accuracy']

# %%
def unpickle(file):
    with open(file, 'rb') as fo:
        myDict = pickle.load(fo, encoding='latin1')
    return myDict


def show_data_item(data):
    for item in data:
        print(item, type(data[item]))


def load_data(data):
    X, CY, FY = data['data'], np.array(data['coarse_labels']), np.array(data['fine_labels'])
    return X, CY, FY


def load_classes(classes):
    C_label, F_label = np.array(classes['coarse_label_names']), np.array(classes['fine_label_names'])
    return C_label, F_label


def preview_images(classnames, label, images,title):
    plt.figure(title,figsize=(20,16))
    for i in range(80):
        plt.subplot(8,10,i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(images[i])
        plt.xlabel(classnames[label[i]], labelpad=2, fontsize=6)
    plt.show()


def create_model(num_classes):
    model = Sequential()

    model.add(Conv2D(filters=128, kernel_size=(11,11), strides=(4,4), activation='relu', input_shape=_CIFAR_IMAGE_SHAPE))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))

    model.add(Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), activation='relu', padding="same"))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2,2)))

    model.add(Flatten())
    model.add(Dense(1024,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1024,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))
    return model


def model_compile(model, opt, loss, metrics):
    model.compile(optimizer=opt, 
                loss=loss,
                metrics= metrics)
    return model

# %%
X_train, CY_train, FY_train = load_data(unpickle('cifar-100-python/train'))
print(X_train.shape, CY_train.shape, FY_train.shape)

# %%
X_train = X_train.reshape(len(X_train),3,_CIFAR_IMAGE_SIZE,_CIFAR_IMAGE_SIZE).transpose(0,2,3,1)
X_train.shape

# %%
X_test, CY_test, FY_test = load_data(unpickle('cifar-100-python/test'))
print(X_test.shape, CY_test.shape, FY_test.shape)

# %%
X_test = X_test.reshape(len(X_test),3,_CIFAR_IMAGE_SIZE,_CIFAR_IMAGE_SIZE).transpose(0,2,3,1)
X_test.shape

# %%
C_label, F_label = load_classes(unpickle('cifar-100-python/meta'))

# %% [markdown]
# # Визуализция датасета

# %% [markdown]
# Вывод изображений с узкими классами

# %%
preview_images(F_label, FY_train, X_train, 'Fine classes')

# %% [markdown]
# Вывод изображений с широкими классами

# %%
preview_images(C_label, CY_train, X_train, 'Coarse classes')

# %% [markdown]
# ## Нормализация X_train, X_test и one-hot encoding 

# %%
X_train = X_train.astype('float32') / 255.0
CY_train_ = to_categorical(CY_train, NUM_C_CLASSES)
FY_train_ = to_categorical(FY_train, NUM_F_CLASSES)

X_test = X_test.astype('float32') / 255.0
CY_test_ = to_categorical(CY_test, NUM_C_CLASSES)
FY_test_ = to_categorical(FY_test, NUM_F_CLASSES)

# %% [markdown]
# # Построение модели

# %% [markdown]
# Модель для классификации на широкие классы

# %%
C_model = model_compile(create_model(NUM_C_CLASSES), opt, loss, metrics)
print('C_model', C_model.summary())

# %% [markdown]
# ### **Выбор гиперпараметров:**
# 
# Здесь используется сверточная нейронная сеть (CNN) для обработки изображений.
# 
# Давайте разберемся с каждым слоем вашей модели и обоснуем выбор гиперпараметров.
# 
# 1. **Слой Conv2D (фильтры=128, размер ядра=(11,11), шаг=(4,4), активация='relu', входной размер=(32,32,3))**:
#     - `filters=128`: Это количество фильтров (или ядер), которые будут применены к входным данным. Большее количество фильтров может помочь изучить более сложные признаки.
#     - `kernel_size=(11,11)`: Это размер ядра свертки. Большие ядра помогают модели извлекать более крупные признаки из входных данных.
#     - `strides=(4,4)`: Это шаг, с которым фильтры перемещаются по входным данным. Больший шаг уменьшает размерность выходных данных.
#     - `activation='relu'`: Функция активации ReLU применяется после свертки, чтобы внести нелинейность в модель.
# 
# 2. **Слой BatchNormalization**:
#     - Использование `BatchNormalization` после сверточных слоев помогает ускорить обучение и улучшить обобщение модели.
# 
# 3. **Слой MaxPooling2D (pool_size=(2,2))**:
#     - Пулинг используется для уменьшения размерности данных и извлечения наиболее важных признаков. `pool_size=(2,2)` означает, что будет использоваться пулинг с размером окна 2x2.
# 
# 4. **Слой Conv2D (фильтры=256, размер ядра=(5,5), шаг=(1,1), активация='relu', padding="same")**:
#     - `filters=256`: Увеличение количества фильтров для изучения более сложных признаков.
#     - `kernel_size=(5,5)`: Уменьшение размера ядра для более локальных признаков.
#     - `strides=(1,1)`: Используется меньший шаг для более тщательного изучения данных.
#     - `padding="same"`: Заполнение нулями вокруг входных данных, чтобы сохранить размерность.
# 
# 5. **Слой BatchNormalization**:
#     - Опять же, BatchNormalization помогает стабилизировать процесс обучения.
# 
# 6. **Слой MaxPooling2D (pool_size=(2,2))**:
#     - Повторный пулинг для уменьшения размерности данных.
# 
# 7. **Слой Flatten**:
#     - Преобразует двумерные данные в одномерный вектор перед подачей их на полносвязные слои.
# 
# 8. **Полносвязный слой Dense (1024 нейрона, активация='relu')**:
#     - 1024 нейрона для извлечения высокоуровневых признаков.
#     - Функция активации ReLU для введения нелинейности.
# 
# 9. **Слой Dropout (0.5)**:
#     - Dropout используется для уменьшения переобучения путем случайного обнуления половины нейронов.
# 
# 10. **Полносвязный слой Dense (1024 нейрона, активация='relu')**:
#     - Аналогично предыдущему полносвязному слою.
# 
# 11. **Слой Dropout (0.5)**:
#     - Дополнительный слой Dropout для уменьшения переобучения.
# 
# 12. **Полносвязный слой Dense (100 нейронов, активация='softmax')**:
#     - Выходной слой с 20 нейронами (20 классов) и функцией активации softmax для многоклассовой классификации. Для модели с 100 классами будет 100 нейронов, т.к. там 100 классов.

# %% [markdown]
# Обучаем модель

# %%
history = C_model.fit(X_train, CY_train_, epochs=EPOCHS, batch_size= BATCH_SIZE)

# %% [markdown]
# Сохраняем обученную модель в C_model.keras

# %%
C_model.save("C_model.keras")
# C_model = keras.models.load_model('C_model.keras')

# %% [markdown]
# Проверяем модель на тестовых данных

# %%
C_test_loss, C_test_acc = C_model.evaluate(X_test, CY_test_)
print('Точность на тестовых данных(широкие классы):', C_test_acc)

# %%
CY_predictions = np.argmax(C_model.predict(X_test), axis = -1)
print(classification_report(CY_test, CY_predictions, target_names = C_label))

# %% [markdown]
# Модель для классификации на узкие классы

# %%
F_model = model_compile(create_model(NUM_F_CLASSES), opt, loss, metrics)
print('F_model', F_model.summary())

# %% [markdown]
# Обучаем модель

# %%
history = F_model.fit(X_train, FY_train_, epochs=EPOCHS, batch_size= BATCH_SIZE)

# %% [markdown]
# Сохраняем обученную модель в F_model.keras

# %%
F_model.save("F_model.keras")
# F_model = keras.models.load_model('F_model.keras')

# %% [markdown]
# Проверяем модель на тестовых данных

# %%
F_test_loss, F_test_acc = F_model.evaluate(X_test, FY_test_)
print('Точность на тестовых данных(широкие классы):', F_test_acc)

# %%
FY_predictions = np.argmax(F_model.predict(X_test), axis = -1)
print(classification_report(FY_test, FY_predictions, target_names = F_label))
F_report = classification_report(FY_test, FY_predictions, target_names=F_label, output_dict = True)

# %%
# Определение по узкой категории широкой
def init_fine_map():
    map = {
        "aquatic_mammals": ["beaver", "dolphin", "otter", "seal", "whale"],
        "fish": ["aquarium_fish", "flatfish", "ray", "shark", "trout"],
        "flowers": ["orchid", "poppy", "rose", "sunflower", "tulip"],
        "food_containers": ["bottle", "bowl", "can", "cup", "plate"],
        "fruit_and_vegetables": ["apple", "mushroom", "orange", "pear", "sweet_pepper"],
        "household_electrical_devices": ["clock", "keyboard", "lamp", "telephone", "television"],
        "household_furniture": ["bed", "chair", "couch", "table", "wardrobe"],
        "insects": ["bee", "beetle", "butterfly", "caterpillar", "cockroach"],
        "large_carnivores": ["bear", "leopard", "lion", "tiger", "wolf"],
        "large_man-made_outdoor_things": ["bridge", "castle", "house", "road", "skyscraper"],
        "large_natural_outdoor_scenes": ["cloud", "forest", "mountain", "plain", "sea"],
        "large_omnivores_and_herbivores": ["camel", "cattle", "chimpanzee", "elephant", "kangaroo"],
        "medium_mammals": ["fox", "porcupine", "possum", "raccoon", "skunk"],
        "non-insect_invertebrates": ["crab", "lobster", "snail", "spider", "worm"],
        "people": ["baby", "boy", "girl", "man", "woman"],
        "reptiles": ["crocodile", "dinosaur", "lizard", "snake", "turtle"],
        "small_mammals": ["hamster", "mouse", "rabbit", "shrew", "squirrel"],
        "trees": ["maple_tree", "oak_tree", "palm_tree", "pine_tree", "willow_tree"],
        "vehicles_1": ["bicycle", "bus", "motorcycle", "pickup_truck", "train"],
        "vehicles_2": ["lawn_mower", "rocket", "streetcar", "tank", "tractor"],
    }
    dict = {}
    for label in F_label:
        for key in map:
            if label in map[key]:
                dict.update({label: key})
                break
    return dict

fine_to_coarse = init_fine_map()

# %%
FC = lambda i: fine_to_coarse[F_label[i]]
CF_predictions = np.array([FC(i) for i in FY_predictions])
C_index = lambda i: np.where(C_label == i)[0][0]

CF_predictions = np.array([C_index(i) for i in CF_predictions])

# %% [markdown]
# ##### Оценка обобщенных предсказаний по узким меткам до метки их широкого класса

# %%
print(classification_report(CY_test, CF_predictions, target_names=C_label))
CF_report = classification_report(CY_test, CF_predictions, target_names=C_label, output_dict = True)

# %% [markdown]
# ##### Оценка предсказаний при обучении на широких метках

# %%
print(classification_report(CY_test, CY_predictions, target_names=C_label))
C_report = classification_report(CY_test, CY_predictions, target_names=C_label, output_dict = True)

# %% [markdown]
# Общая точность предсказания в обоих случаях одинакова и составляет 0.44.

# %% [markdown]
# #### Исследование с помощью графиков метрики предсказания для каких узких классов более всего отличаются от метрик их более широких классов

# %%
# Точность распознавания узкого класса
F_precision =  lambda i: F_report[i]['precision']
F_precisions = np.array([F_precision(i) for i in F_label])

#Точность распознавания широкого класса по узкому
CF_precision =  lambda i: CF_report[fine_to_coarse[i]]['precision']
CF_precisions = np.array([CF_precision(i) for i in F_label])

# Точность распознавания соответствующего широкого класса
C_precision =  lambda i: C_report[fine_to_coarse[i]]['precision']
C_precisions = np.array([C_precision(i) for i in F_label])

# %%
df = pd.DataFrame(
    {
        'label': F_label,
        'c_label': np.array([fine_to_coarse[i] for i in F_label]),
        'F_CF': F_precisions - CF_precisions,
        'F_C': F_precisions - C_precisions,
        'CF_C': CF_precisions - C_precisions,
    })

# График различия точности распознавания узкого класса и точности распознавания широкого класса по узкому
df.sort_values(by=['c_label', 'F_CF'],ascending=True).plot.barh(y='F_CF', x='label', legend=False, figsize=(6, 16), ylabel='')

# %% [markdown]
# График показывает различия точности распознавания узкого класса и точности распознавания широкого класса по узкому и позволяет сделать вывод, насколько точно определяется узкий класс в рамках широкого. Наибольший разброс имеют элементы широкого класса деревьев (trees) и людей (people), это говорит о том, что конкретный вид деревьев и людей между собой данная сеть определяет плохо. Наименьшее отклонение имеют следующие узкие классы dinosaur, turtle, worm, crab, porcupine, castle, lion, cockroach, keyboard, bottle, sunflower, whale, aquarium_fish сеть определяет хорошо. 

# %%
# График различия точности распознавания узкого класса и точности распознавания широкого класса
df.sort_values(by=['c_label', 'F_C'],ascending=True).plot.barh(y='F_C', x='label', legend=False, figsize=(6, 16), ylabel='')

# %% [markdown]
# График показывает различия точности распознавания узкого класса и точности распознавания широкого класса и позволяет сделать вывод о том, что модель определяет лучше широкие классы или узкие. Например, изображения dinosaur, turtle определяются с примерно одинаковой точностью, как в случае распознавания по узкому классу, так и по широкому.

# %%
# График различия точности распознавания широкого класса по узкому и точности распознавания широкого класса
df.sort_values(by=['c_label', 'CF_C'],ascending=True).plot.barh(y='CF_C', x='label', legend=False, figsize=(6, 16), ylabel='')

# %% [markdown]
# График различия точности распознавания широкого класса по узкому и точности распознавания широкого класса позволяет сделать вывод о том, какие категории лучше определяются при обучении на узких классах, а какие на широких. На узких классах лучше определяются (имеют положительное значение на графике) vehicles_2,vehicles_1, trees, small_mammals, reptiles, people, medium_mammals, large_omnivores_and_herbivores, large natural outdoor scenes, large_man-made_outdoor_things, large_carnivores, insects, fruit_and_vegetables, food_containers, fish, aquatic_mammals. Остальные классы лучше определяются при обучении на широких классах.


